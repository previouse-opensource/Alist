package aria2

type OfflineDownload struct {
	Gid     string
	DstPath string
	URI     string
}
